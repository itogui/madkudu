# URL's
1. API : http://localhost:8080/swagger-ui.html
2. Storm UI : http://localhost:49080/index.html


# Hints :
1. Storm introduction :
http://fabulous-lab.com/blog/2014/11/12/introduction-aux-concepts-dapache-storm/
2. Setting up topology :
http://www.haroldnguyen.com/blog/2015/01/setting-up-storm-and-running-your-first-topology/
https://www.tutorialspoint.com/apache_kafka/apache_kafka_integration_storm.htm
https://www.tutorialspoint.com/apache_storm/apache_storm_working_example.htm
