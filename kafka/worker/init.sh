#!/bin/bash

#kafka_bin_dir="/usr/bin"
#zookeeper_server="127.0.0.1:2181"
#kafka_bootsrap_server="127.0.0.1:9092"
#topic="user_logs"

kafka_bin_dir=$1
zookeeper_server=$2
kafka_bootsrap_server=$3
topic=$4

while ! ping -c 1 -n -w 1 kafka1 &> /dev/null
do
    echo "..."
done
echo "[Connected to zoo/kafka]"

sleep 60

echo "[Processing Kafka : init Topic]"
echo "======================================="
$kafka_bin_dir/kafka-topics --create --zookeeper $zookeeper_server --replication-factor 1 --partitions 1 --topic $topic
echo "	> [Processing Topic : $topic Created]"
echo "======================================="
echo "[Processing Kafka : read Topic]"
echo "======================================="
$kafka_bin_dir/kafka-console-consumer --bootstrap-server $kafka_bootsrap_server --topic $topic --from-beginning
echo "======================================="