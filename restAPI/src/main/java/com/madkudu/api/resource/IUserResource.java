package com.madkudu.api.resource;
import org.springframework.http.ResponseEntity;

import com.madkudu.api.entitie.BehavioralProfile;

public interface IUserResource {

	ResponseEntity<BehavioralProfile> getBehavioralProfile(String user_id);

}
