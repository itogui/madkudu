package com.madkudu.api.resource.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.madkudu.api.entitie.BehavioralProfile;
import com.madkudu.api.resource.IUserResource;
import com.madkudu.api.service.impl.UserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/madkudu/user/v1")
@Api(value = "USER Behavioral Profile API")
public class UserResource implements IUserResource {

	@Autowired 
	UserService userService;
	
	@Override
	@RequestMapping(value = "/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<BehavioralProfile> getBehavioralProfile(
			@PathVariable String user_id) {
		return ResponseEntity.ok(userService.getBehavioralProfile(user_id));
	}

}
