package com.madkudu.api.resource;

import org.springframework.http.ResponseEntity;

import com.madkudu.api.entitie.PageView;

public interface IPageResource {

	ResponseEntity<Boolean> postPageView(PageView pageView);

}
