package com.madkudu.api.resource.impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.madkudu.api.entitie.PageView;
import com.madkudu.api.resource.IPageResource;
import com.madkudu.api.service.impl.PageService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/madkudu/page/v1")
@Api(value = "Page API")
public class PageResource implements IPageResource{

	@Autowired
	PageService pageService;
	
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Boolean> postPageView(@RequestBody PageView pageView) {
		return ResponseEntity.ok(pageService.postPageView(pageView));
	}

}
