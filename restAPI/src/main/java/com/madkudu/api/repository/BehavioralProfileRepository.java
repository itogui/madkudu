package com.madkudu.api.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import com.madkudu.api.entitie.BehavioralProfile;

@Repository
public interface BehavioralProfileRepository extends CassandraRepository<BehavioralProfile, String> {

}
