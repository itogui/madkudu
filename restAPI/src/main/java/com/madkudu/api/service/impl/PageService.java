package com.madkudu.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.madkudu.api.entitie.PageView;
import com.madkudu.api.service.IPageService;

@Service
public class PageService implements IPageService{

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	private KafkaService kafkaService;
	
	@Value("${kafka.topicName}")
	private String topicName;

	@Autowired
	public boolean postPageView(PageView pageView) {
		kafkaService.saveMessageToTopic(topicName, serializePageView(pageView));
		return true;
	}

	public String serializePageView(PageView pageView) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(pageView);
		} catch (JsonProcessingException e) {
			return null;
		}
	}

	public PageView deserializePageView(String pageView) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(pageView, PageView.class);
		} catch (IOException e) {
			return null;
		}
	}

}
