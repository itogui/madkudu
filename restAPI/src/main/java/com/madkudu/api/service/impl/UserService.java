package com.madkudu.api.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.madkudu.api.entitie.BehavioralProfile;
import com.madkudu.api.repository.BehavioralProfileRepository;
import com.madkudu.api.service.IUserService;

@Service
public class UserService implements IUserService{

	@Autowired
	BehavioralProfileRepository behavioralProfileRepository;
	
	@Override
	public BehavioralProfile getBehavioralProfile(String user_id) {
		return behavioralProfileRepository.findById(user_id).get();
	}

}
