package com.madkudu.api.service.impl;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.madkudu.api.service.IKafkaService;

@Service
public class KafkaService implements IKafkaService {

	private KafkaTemplate<String, String> kafkaTemplate;

	public KafkaService(KafkaTemplate<String, String> kafkaT) {
		this.kafkaTemplate = kafkaT;
	}

	@Override
	public void saveMessageToTopic(String topicName, String message) {
		kafkaTemplate.send(topicName, message);
	}

}
