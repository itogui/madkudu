package com.madkudu.api.service;

public interface IKafkaService {
	public void saveMessageToTopic(String topicName, String message);
}
