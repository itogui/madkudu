package com.madkudu.api.service;

import com.madkudu.api.entitie.BehavioralProfile;

public interface IUserService {

	BehavioralProfile getBehavioralProfile(String user_id);

}
