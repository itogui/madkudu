package com.madkudu.api.entitie;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class BehavioralProfile {
	
	@PrimaryKey
	private String user_id;
	
	@Column 
	private int number_pages_viewed_in_the_last_7_days;
	
	@Column
	private int time_spent_on_site_in_last_7_days;
	
	@Column
	private int number_of_days_active_in_last_7_days;
	
	@Column
	private String most_viewed_page_in_last_7_days;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getNumber_pages_viewed_in_the_last_7_days() {
		return number_pages_viewed_in_the_last_7_days;
	}
	public void setNumber_pages_viewed_in_the_last_7_days(int number_pages_viewed_in_the_last_7_days) {
		this.number_pages_viewed_in_the_last_7_days = number_pages_viewed_in_the_last_7_days;
	}
	public int getTime_spent_on_site_in_last_7_days() {
		return time_spent_on_site_in_last_7_days;
	}
	public void setTime_spent_on_site_in_last_7_days(int time_spent_on_site_in_last_7_days) {
		this.time_spent_on_site_in_last_7_days = time_spent_on_site_in_last_7_days;
	}
	public int getNumber_of_days_active_in_last_7_days() {
		return number_of_days_active_in_last_7_days;
	}
	public void setNumber_of_days_active_in_last_7_days(int number_of_days_active_in_last_7_days) {
		this.number_of_days_active_in_last_7_days = number_of_days_active_in_last_7_days;
	}
	public String getMost_viewed_page_in_last_7_days() {
		return most_viewed_page_in_last_7_days;
	}
	public void setMost_viewed_page_in_last_7_days(String most_viewed_page_in_last_7_days) {
		this.most_viewed_page_in_last_7_days = most_viewed_page_in_last_7_days;
	}
	@Override
	public String toString() {
		return "BehavioralProfile [user_id=" + user_id + ", number_pages_viewed_in_the_last_7_days="
				+ number_pages_viewed_in_the_last_7_days + ", time_spent_on_site_in_last_7_days="
				+ time_spent_on_site_in_last_7_days + ", number_of_days_active_in_last_7_days="
				+ number_of_days_active_in_last_7_days + ", most_viewed_page_in_last_7_days="
				+ most_viewed_page_in_last_7_days + "]";
	}
	public BehavioralProfile(String user_id, int number_pages_viewed_in_the_last_7_days,
			int time_spent_on_site_in_last_7_days, int number_of_days_active_in_last_7_days,
			String most_viewed_page_in_last_7_days) {
		super();
		this.user_id = user_id;
		this.number_pages_viewed_in_the_last_7_days = number_pages_viewed_in_the_last_7_days;
		this.time_spent_on_site_in_last_7_days = time_spent_on_site_in_last_7_days;
		this.number_of_days_active_in_last_7_days = number_of_days_active_in_last_7_days;
		this.most_viewed_page_in_last_7_days = most_viewed_page_in_last_7_days;
	}
	public BehavioralProfile() {
		super();
	}
	
}
