package com.madkudu.api.entitie;

import java.util.Date;

import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.stereotype.Component;

@Component
public class PageView {
	String user_id;
	String name;
	Date timestamp;
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "PageView [user_id=" + user_id + ", name=" + name + ", timestamp=" + timestamp + "]";
	}
	public PageView(String user_id, String name, Date timestamp) {
		super();
		this.user_id = user_id;
		this.name = name;
		this.timestamp = timestamp;
	}
	public PageView() {
		super();
	}
	
}
